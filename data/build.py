from pathlib import Path

import numpy as np
from batchup import data_source
from torchvision.datasets import MNIST

DATA_DIR = Path(__file__).parent.parent / "datasets" / "MNIST"


def dense_to_one_hot(y, class_count):
    return np.eye(class_count)[y]


def build_training_data_source(cfg):
    ds_train = MNIST(DATA_DIR, train=True, download=True)
    train_x = (
        ds_train.data.reshape([-1, *cfg.input.size]).numpy().astype(np.float64) / 255
    )
    train_y = ds_train.targets.numpy()

    num_training_samples = int(cfg.input.train_ratio * len(train_x))
    train_x = train_x[:num_training_samples]
    train_y = train_y[:num_training_samples]

    # Normalize the data
    train_mean = train_x.mean()
    train_std = train_x.std()
    train_x = (train_x - train_mean) / train_std

    train_y = dense_to_one_hot(train_y, 10)

    return data_source.ArrayDataSource([train_x, train_y], repeats=cfg.train.max_epochs)


def build_validation_data_source(cfg):
    ds_train = MNIST(DATA_DIR, train=True, download=True)
    train_x = (
        ds_train.data.reshape([-1, *cfg.input.size]).numpy().astype(np.float64) / 255
    )
    train_y = ds_train.targets.numpy()

    num_training_samples = int(cfg.input.train_ratio * len(train_x))
    valid_x = train_x[num_training_samples:]
    valid_y = train_y[num_training_samples:]

    # Normalize the data
    train_mean = train_x.mean()
    train_std = train_x.std()
    valid_x = (valid_x - train_mean) / train_std

    valid_y = dense_to_one_hot(valid_y, 10)

    return data_source.ArrayDataSource([valid_x, valid_y])
