import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.metrics import ConfusionMatrixDisplay

from metrics.scores import accuracy, confusion_matrix, f1_score, precision, recall


def save_confusion_matrix(cmat, path):
    disp = ConfusionMatrixDisplay(cmat)
    disp.plot()
    disp.ax_.set_title("MNIST confusion matrix")

    disp.figure_.savefig(path)
    plt.close(disp.figure_)


def calculate_scores(cmat):
    scores = {
        "label": np.arange(len(cmat)),
        "accuracy": accuracy(cmat),
        "precision": precision(cmat),
        "recall": recall(cmat),
        "f1_score": f1_score(cmat),
    }
    return scores


def evaluate_classification(model, data_source, save_cmat=True):
    y_true = np.array([], dtype=np.int32)
    y_pred = np.array([], dtype=np.int32)

    for batch_x, batch_y in data_source.batch_iterator(batch_size=50):
        logits = model.forward(batch_x)

        y_true_batch = np.argmax(logits, 1)
        y_pred_batch = np.argmax(batch_y, 1)

        y_true = np.concatenate((y_true, y_true_batch), axis=None)
        y_pred = np.concatenate((y_pred, y_pred_batch), axis=None)

    cmat = confusion_matrix(y_pred, y_true)
    scores = calculate_scores(cmat)
    scores_df = pd.DataFrame(scores)
    print(scores_df.to_markdown(headers="keys", tablefmt="psql", index=False))

    if save_cmat:
        save_confusion_matrix(cmat, "results/confusion_matrix.png")

    valid_acc = np.trace(cmat) / data_source.num_samples() * 100
    return {
        "accuracy": valid_acc,
        "f1_score": scores["f1_score"].mean(),
        "precision": scores["precision"].mean(),
        "recall": scores["recall"].mean(),
    }
