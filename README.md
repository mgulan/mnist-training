## Getting Started
After cloning the project, run the commands listed below to create a Python [virtual environment](https://docs.python.org/3/library/venv.html).

> **Note:** Since the code is written in Python 3.8 it is advisable to use that version to run the commands.

```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

If you want to use Docker to experiment with models, follow these instructions:

```bash
cd docker/
docker build -t mnist-training .
docker run -it --name=mnist-training mnist-training
```

## Training
There's a script [train.py](train.py) which is used to train the network. It expects an argument `--config` containing the path to the config file. The example of the config file can be found [here](configs/config.yaml).

**Example:**
```bash
python train.py --config configs/config.yaml
```

## Results
In this experiment, convolutional based network has been used to classify MNIST images. The network comprises of two convolutional blocks (*convolutional layer followed by max pooling layer and ReLU activation function*) and two fully connected layers.

The network was trained for $10$ epochs on the $54,000$ training images. Besides training images, the dataset also contains $6,000$ validation images. I used that set for evaluating the model after every epoch. The evaluation metrics used in this experiment were: accuracy, precision, recall and f1-score. The model with the best accuracy on the validation set was chosen as a best model.

The chosen model achieves an accuracy of $99.10$% on the validation set. After chosing the model, it was evaluated on test set to see the final results. In the table below, you can see the results of te chosen model label-by-label.

|   label |   accuracy |   precision |   recall |   f1_score |
|--------:|-----------:|------------:|---------:|-----------:|
|  0      |     0.9983 |      0.9949 |   0.9878 |     0.9913 |
|  1      |     0.9993 |      0.9956 |   0.9982 |     0.9969 |
|  2      |     0.9983 |      0.9951 |   0.9884 |     0.9917 |
|  3      |     0.9985 |      0.9901 |   0.9950 |     0.9926 |
|  4      |     0.9981 |      0.9908 |   0.9898 |     0.9903 |
|  5      |     0.9974 |      0.9932 |   0.9776 |     0.9853 |
|  6      |     0.9980 |      0.9885 |   0.9906 |     0.9896 |
|  7      |     0.9977 |      0.9827 |   0.9951 |     0.9889 |
|  8      |     0.9969 |      0.9806 |   0.9877 |     0.9841 |
|  9      |     0.9971 |      0.9861 |   0.9851 |     0.9856 |

Here's also the confusion matrix in which you can see how the model behaves label-by-label.
![Confusion matrix](images/confusion_matrix.png)

## TODO:
- [ ] Implement reading MNIST data (instead of using third-party library)
- [ ] Save the best model
- [ ] Implement learning rate scheduling
