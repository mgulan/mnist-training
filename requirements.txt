omegaconf==2.3.0
batchup==0.2.3
numpy==1.24.4
pandas==2.0.3
torchvision==0.15.2
scikit-learn==1.3.0
tabulate==0.9.0
matplotlib==3.7.2
