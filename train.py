import argparse

from omegaconf import OmegaConf

from config import get_default_config
from trainer.trainer import DefaultTrainer


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", default="", metavar="FILE", help="config file path")
    return parser


def _get_config(config_file):
    cfg = get_default_config()

    if config_file:
        cfg = OmegaConf.merge(cfg, OmegaConf.load(config_file))

    return cfg


def main(args):
    cfg = _get_config(args.config)

    trainer = DefaultTrainer(cfg)
    trainer.train()


if __name__ == "__main__":
    args = _get_argument_parser().parse_args()
    main(args)
