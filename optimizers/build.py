from optimizers.sgd import SGD


def build_optimizer(cfg):
    return SGD(cfg)
