from abc import ABCMeta, abstractmethod


class Optimizer(metaclass=ABCMeta):
    def __init__(self, cfg):
        self.cfg = cfg

    @abstractmethod
    def step(self, grads):
        pass
