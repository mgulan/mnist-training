from optimizers.base import Optimizer


class SGD(Optimizer):
    def __init__(self, cfg):
        super().__init__(cfg)

    def step(self, grads):
        lr = self.cfg.train.learning_rate
        for layer_grads in grads:
            for i in range(len(layer_grads) - 1):
                params = layer_grads[i][0]
                grads = layer_grads[i][1]
                params -= lr * grads
