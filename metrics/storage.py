from collections import defaultdict

from metrics.utils import AverageMeter

_METRIC_STORAGE = None


def get_metric_storage():
    global _METRIC_STORAGE
    if _METRIC_STORAGE is None:
        _METRIC_STORAGE = MetricStorage()
    return _METRIC_STORAGE


class ConsoleWriter:
    def write(self):
        storage = get_metric_storage()
        step = storage.step
        loss = storage.meter("loss").avg
        print(f"step: {step}  loss: {loss:.4f}")


class MetricStorage:
    def __init__(self):
        self._meters = defaultdict(AverageMeter)
        self._step = 0

    def add_metric(self, name, value):
        meter = self._meters[name]
        value = float(value)
        meter.update(value)

    def meter(self, name):
        return self._meters.get(name, None)

    @property
    def step(self):
        return self._step

    @step.setter
    def step(self, val):
        self._step = int(val)
