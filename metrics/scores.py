import numpy as np


def confusion_matrix(y_true, y_pred, n_classes=None):
    if n_classes is None:
        n_classes = max(np.max(y_true), np.max(y_pred)) + 1

    y_true = y_true.reshape(-1)
    y_pred = y_pred.reshape(-1)

    idx = n_classes * y_true + y_pred
    cmat = np.bincount(idx, minlength=n_classes**2)
    cmat = cmat.reshape(n_classes, n_classes)
    return cmat


def accuracy(cmat):
    cmat_sum = np.sum(cmat)

    tp = np.diag(cmat)
    fp = np.sum(cmat, axis=0) - tp
    fn = np.sum(cmat, axis=1) - tp

    return (cmat_sum - fp - fn) / cmat_sum


def precision(cmat):
    return np.diag(cmat) / np.sum(cmat, axis=0)


def recall(cmat):
    return np.diag(cmat) / np.sum(cmat, axis=1)


def f1_score(cmat):
    precision_score = precision(cmat)
    recall_score = recall(cmat)

    return 2 * (precision_score * recall_score) / (precision_score + recall_score)
