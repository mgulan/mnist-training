import numpy as np

import model.layers as layers
from model.model import Model


def build_model(cfg):
    net = []
    inputs = np.random.randn(cfg.train.batch_size, 1, 28, 28)
    net += [layers.Convolution(inputs, 16, 5, "conv1")]
    net += [layers.MaxPooling(net[-1], "pool1")]
    net += [layers.ReLU(net[-1], "relu1")]
    net += [layers.Convolution(net[-1], 32, 5, "conv2")]
    net += [layers.MaxPooling(net[-1], "pool2")]
    net += [layers.ReLU(net[-1], "relu2")]
    net += [layers.Flatten(net[-1], "flatten3")]
    net += [layers.FC(net[-1], 512, "fc3")]
    net += [layers.ReLU(net[-1], "relu3")]
    net += [layers.FC(net[-1], 10, "logits")]

    return Model(net, layers.SoftmaxCrossEntropyWithLogits())
