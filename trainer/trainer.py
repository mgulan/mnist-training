from functools import partial
from pathlib import Path

import trainer.hooks as hooks
from data.build import build_training_data_source, build_validation_data_source
from eval.classification import evaluate_classification
from model import build_model
from optimizers import build_optimizer
from trainer.base import AbstractTrainer, ConcreteTrainer


class DefaultTrainer(AbstractTrainer):
    def __init__(self, cfg):
        super().__init__()

        model = build_model(cfg)
        optimizer = build_optimizer(cfg)
        data_source = build_training_data_source(cfg)

        self._trainer = ConcreteTrainer(model, data_source, optimizer)

        self.model = model
        self.num_steps = data_source.num_samples() // cfg.train.batch_size
        self.cfg = cfg

        self.register_hooks(self.get_hooks())

    def get_hooks(self):
        cfg = self.cfg.copy()
        evaluate_function = partial(self.test, cfg=cfg, model=self.model)
        num_steps_per_epoch = self.num_steps // cfg.train.max_epochs

        return [
            hooks.PeriodicPrinter(period=90),
            hooks.PeriodicEvalutor(evaluate_function, period=num_steps_per_epoch),
            hooks.ModelSaver(self.model, Path("results"), period=num_steps_per_epoch),
        ]

    def train(self):
        super().train(self.num_steps)

    def run_step(self):
        self._trainer.step = self.step
        self._trainer.run_step()

    @classmethod
    def test(cls, cfg, model):
        data_source = build_validation_data_source(cfg)

        return evaluate_classification(model, data_source)
