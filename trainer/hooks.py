import pickle

from metrics.storage import ConsoleWriter
from trainer.base import AbstractHook


class PeriodicPrinter(AbstractHook):
    def __init__(self, period=20):
        self._writer = ConsoleWriter()
        self._period = period

    def after_step(self):
        if (self.trainer.step + 1) % self._period == 0 or (
            self.trainer.step == self.trainer.num_steps - 1
        ):
            self._writer.write()


class ModelSaver(AbstractHook):
    def __init__(self, model, path, period):
        self._model = model
        self._path = path
        self._period = period

    def _save_model(self):
        file_path = self._path / f"model_step_{self.trainer.step}.pkl"
        with file_path.open("wb") as file:
            pickle.dump(self._model, file)
        print(f"Model saved to the file {file_path}")

    def after_step(self):
        next_step = self.trainer.step + 1
        if next_step % self._period == 0 and next_step != self.trainer.num_steps:
            self._save_model()

    def after_train(self):
        if self.trainer.step + 1 >= self.trainer.num_steps:
            self._save_model()


class PeriodicEvalutor(AbstractHook):
    def __init__(self, eval_function, period):
        self._eval_function = eval_function
        self._period = period

    def _evaluate(self):
        print("\nRunning evaluation:")
        results = self._eval_function()

        if results:
            print(
                f"accuracy: {results['accuracy']:.2f} ",
                f"precision {results['precision']:.4f}  ",
                f"recall {results['recall']:.4f}  ",
                f"f1_score {results['f1_score']:.4f}\n",
            )

    def after_step(self):
        next_step = self.trainer.step + 1
        if next_step % self._period == 0 and next_step != self.trainer.num_steps:
            self._evaluate()

    def after_train(self):
        if self.trainer.step + 1 >= self.trainer.num_steps:
            self._evaluate()
