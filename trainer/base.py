import weakref

from metrics.storage import get_metric_storage
from model import backward_pass


class AbstractHook:
    trainer = None

    def before_train(self):
        pass

    def after_train(self):
        pass

    def before_step(self):
        pass

    def after_step(self):
        pass


class AbstractTrainer:
    def __init__(self) -> None:
        self._hooks = []
        self.step = 0

    def register_hooks(self, hooks) -> None:
        for h in hooks:
            h.trainer = weakref.proxy(self)
        self._hooks.extend(hooks)

    def train(self, num_steps):
        self.step = 0
        self.num_steps = num_steps
        self.storage = get_metric_storage()

        try:
            self.before_train()
            for self.step in range(num_steps):
                self.before_step()
                self.run_step()
                self.after_step()
            self.step += 1
        finally:
            self.after_train()

    def before_train(self):
        for h in self._hooks:
            h.before_train()

    def after_train(self):
        self.storage.step = self.step
        for h in self._hooks:
            h.after_train()

    def before_step(self):
        self.storage.step = self.step

        for h in self._hooks:
            h.before_step()

    def after_backward(self):
        for h in self._hooks:
            h.after_backward()

    def after_step(self):
        for h in self._hooks:
            h.after_step()

    def run_step(self):
        raise NotImplementedError


class ConcreteTrainer(AbstractTrainer):
    def __init__(self, model, data_source, optimizer):
        super().__init__()

        self.model = model
        self.data_source = data_source
        self.optimizer = optimizer
        self._data_iter = self.data_source.batch_iterator(batch_size=50, shuffle=True)

    def run_step(self):
        data = next(self._data_iter)

        logits = self.model.forward(data[0])
        loss_val = self.model.loss.forward(logits, data[1])

        grads = backward_pass(self.model, self.model.loss, logits, data[1])

        get_metric_storage().add_metric("loss", loss_val)

        self.optimizer.step(grads)
