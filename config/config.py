from omegaconf import OmegaConf

_C = OmegaConf.create()

_C.dataset = OmegaConf.create()
_C.dataset.path = "datasets/MNIST"

_C.input = OmegaConf.create()
_C.input.size = (1, 28, 28)
_C.input.train_ratio = 0.9

_C.train = OmegaConf.create()
_C.train.batch_size = 50
_C.train.shuffle = True
_C.train.learning_rate = 1e-5
_C.train.max_epochs = 50
