import copy


def get_default_config():
    from .config import _C

    return copy.deepcopy(_C)
